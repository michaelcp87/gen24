# gen24

First of all, thanks to "Flo" and "Dala" for the great job with reversing the registers involved.


## Caution: Use At Your Own Risk
HV batteries can be dangerous, and applying wrong configuration or running code in an unintended way can cause danger to yourself or the battery. 
Use the code, modify it, but don't blame me :-)

## Integrating a HV battery with a Gen24 inverter
There is currently a big limitation in the number of batteries supported with Gen24. The files in this repo can be used to circumvent this by emulating a BYD battery over RS485. As long as the connected battery lies within the voltage specs for Gen24 you should be good to go.


## Scripts
Requires: pymodbus (I'm runnning 2.5.3 on python 3.7)
Most of the scripts below are modified ones from pymodbus pages.


### server.py
This one emulates a BYD battery with a static SoC. When run, a battery should pop as connected in the Gen24 webgui.
You need a RS485 interface connected to Gen24.


### server_db.py
Same as server_db.py, but takes values from a database which makes it more applicable in real life....where SoC changes.
If you have a BMS, values from that one should be injected in the database that this script reads from.
_You will need to adjust this to your own setup, this only serves as a base or food for thought!_


### create_db.py
Creates the db that is used by server_db.py


### client.py
This one connects over tcp to a Gen24 and controls charging/discharging.
Only IP connectivity and configuration in Gen24 are required to enable communication over modbus/tcp.

First tests shows that you can control charging and discharging, together with a setting for power. The latter setting is a bit weird since its a percentage of the capacity of the battery (from 200-registers during RS485 battery setup). Nevertheless the wanted power can be computed.

