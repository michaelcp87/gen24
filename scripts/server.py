#!/usr/bin/env python
"""
Pymodbus Synchronous Server Example - modified to spoof a BYD to GEN24 Per Carlén
--------------------------------------------------------------------------

"""
# --------------------------------------------------------------------------- #
# import the various server implementations
# --------------------------------------------------------------------------- #
from pymodbus.server.sync import StartSerialServer

from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock, ModbusSparseDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext

from pymodbus.transaction import ModbusRtuFramer, ModbusBinaryFramer
# --------------------------------------------------------------------------- #
# configure the service logging
# --------------------------------------------------------------------------- #
import logging
FORMAT = ('%(asctime)-15s %(threadName)-15s'
          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.DEBUG)

def Str2Int32(s):
    a1 = ord(s[0:1])
    if len(s) == 2:
        a2 = ord(s[1:2])
    else:
        a2 = 0
    ret = a1*256 + a2
    return ret

def run_server():
    # ----------------------------------------------------------------------- #
    # initialize your data store
    # ----------------------------------------------------------------------- #
    # Registers used: 101:["SI", 1], 103:["BY", "D ", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 119:["BY", "D ", "Ba", "tt", "er", "y-", "Bo", "x ", "Pr", "em", "iu", "m ", "HV", 0, 0, 0], 135:["5.", "0", 0, 0, 0, 0, 0, 0, "3.", "16", 0, 0, 0, 0, 0, 0], 151:["P0", "30", "T0", "20", "Z2", "00", "81", "03", "48", "80", " ", " ", 0, 0, 0, 0], 167:[1, 0], 201:[0, 0, 11059, 10240, 10240, 2336, 1600, 13312, 10, 13312, 10, 0, 0], 301:[1, 0, 128, 470, 10948, 514, 0, 8352, 0, 0, 2058, 0, 60, 70, 0, 0, 16, 22741, 0, 0, 13, 52064, 80, 9900], 401:[0]*20, 1001:[0]*100, 12289:[0]*768})

    p101 = [ Str2Int32("SI") , 1 ]
    p103 = [ Str2Int32("BY"), Str2Int32("D"), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    p119 = [ Str2Int32("BY"), Str2Int32("D "), Str2Int32("Ba"), Str2Int32("tt"), Str2Int32("er"), Str2Int32("y-"), Str2Int32("Bo"), Str2Int32("x "), Str2Int32("Pr"), Str2Int32("em"), Str2Int32("iu"), Str2Int32("m "), Str2Int32("HV"), 0, 0, 0]
    p135 = [ Str2Int32("5."), Str2Int32("0"), 0, 0, 0, 0, 0, 0, Str2Int32("3."), Str2Int32("16"), 0, 0, 0, 0, 0, 0]
    p151 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    p167 = [ 1, 0 ]
    # tweaked to Leaf, only 200W, 180-220V
    p201 = [0, 0, 12000, 200, 200, 2200, 1800, 300, 10, 300, 10, 0, 0]
    # from flo:
    # p201 = [0, 0, 11059, 10240, 10240, 2336, 1600, 13312, 10, 13312, 10, 0, 0]
    p301 = [1, 0, 128, 7500, 10948, 514, 0, 8352, 0, 0, 2058, 0, 60, 70, 0, 0, 16, 22741, 0, 0, 13, 52064, 80, 9900]

    block = ModbusSparseDataBlock({ 101: p101, 103:p103, 119:p119, 135:p135, 151:p151, 167:p167, 201:p201, 301:p301, 401:[0]*20, 1001:[0]*100, 12289:[0]*768 })
    store = ModbusSlaveContext(di=block, co=block, hr=block, ir=block)
    context = ModbusServerContext(slaves= store, single=True)
    identity = ModbusDeviceIdentification()
    # RTU:
    StartSerialServer(context, framer=ModbusRtuFramer, identity=identity,port='/dev/ttyUSB0', timeout=.005, baudrate=9600)


if __name__ == "__main__":
    run_server()
