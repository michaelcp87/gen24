#!/usr/bin/env python
'''
Pymodbus Synchronous Client Examples
--------------------------------------------------------------------------

The following is an example of how to use the synchronous modbus client
implementation from pymodbus.

Modified to connect to Fronius Gen24, Per Carlen
'''
from pymodbus.client.sync import ModbusTcpClient as ModbusClient

import logging
#logging.basicConfig()
#log = logging.getLogger()
#log.setLevel(logging.DEBUG)


def print_rr(lbl,rr):
  print(lbl + ":",end='')
  for x in rr.registers:
    s = x.to_bytes(2,"big").hex()
    print(s + " (" + str(x) + ")",end='')
  print("")



client = ModbusClient('192.168.2.5', port=502)
client.connect()
rr = client.write_registers(40350, 2200, unit=0x01) # soc 22%

#rr = client.write_registers(40348, 2, unit=0x01) # Charge limit
#rr = client.write_registers(40355, 64500, unit=0x01) # outrte 
#rr = client.write_registers(40356, 0, unit=0x01) # inwrte 

rr = client.write_registers(40348, 1, unit=0x01) # disCharge limit
rr = client.write_registers(40355, 0, unit=0x01) # outrte 
rr = client.write_registers(40356, 64300, unit=0x01) # inwrte 

rr = client.read_holding_registers(40345, 1, unit=0x01)
print_rr("WChamax",rr)
rr = client.read_holding_registers(40348, 1, unit=0x01)
print_rr("StorCtl_Mod",rr)
rr = client.read_holding_registers(40354, 1, unit=0x01)
print_rr("ChaSt",rr)
rr = client.read_holding_registers(40360, 1, unit=0x01)
print_rr("CHaGriSet",rr)
rr = client.read_holding_registers(40355, 1, unit=0x01)
print_rr("OutWRte",rr)
rr = client.read_holding_registers(40356, 1, unit=0x01)
print_rr("InWRte",rr)
rr = client.read_holding_registers(40350, 1, unit=0x01)
print_rr("MinRsvPct",rr)
rr = client.read_holding_registers(40351, 1, unit=0x01)
print_rr("ChaState",rr)
rr = client.read_holding_registers(40232, 1, unit=0x01)
print_rr("WMaxLimPct",rr)
rr = client.read_holding_registers(40107, 1, unit=0x01)
print_rr("St",rr)
client.close()
